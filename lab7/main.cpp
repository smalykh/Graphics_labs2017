#include <stdio.h>
#include "fops.h"
#include <math.h>
#include <vector>
#include <algorithm>
using namespace std;

#define BIT_DEPTH 	8
#define COLORTYPE 	0


const char *filename		= "lena.png";
const char *filename_out	= "file1_out.png";
const char *filename_poly	= "poly.png";

#define POLY_WIDTH	10
#define POLY_HEIGHT	5


#define WINDOW_SIZE		2
#define GLOBAL_THRESHOLD	150

int threshold(int val, int threshold)
{
	return (val < threshold)? 0 : 1;
}
int qClassify(png_img *im, int x, int y)
{
	int sum =0;
	sum += threshold(im->getPixel(x    , y    ), GLOBAL_THRESHOLD);
	sum += threshold(im->getPixel(x + 1, y    ), GLOBAL_THRESHOLD);
	sum += threshold(im->getPixel(x    , y + 1), GLOBAL_THRESHOLD);
	sum += threshold(im->getPixel(x + 1, y + 1), GLOBAL_THRESHOLD);
	if(x == 48 && y == 48) printf("sum = %d\n\n", sum);
	if(sum == 0) return 0;
	if(sum == 1) return 1;
	if(sum == 3) return 3;
	if(sum == 4) return 4;
	
	if((threshold(im->getPixel(x + 1, y    ), GLOBAL_THRESHOLD) &&
	    threshold(im->getPixel(x    , y + 1), GLOBAL_THRESHOLD)) ||
 	   (threshold(im->getPixel(x    , y    ), GLOBAL_THRESHOLD) &&
	    threshold(im->getPixel(x + 1, y + 1), GLOBAL_THRESHOLD))) 
		return 5;
	else	return 2;


}

int connected_domain4_counting(png_img * im)
{
	int qCount[6];
	memset(qCount, 0, sizeof(int)*6);
	int rigth_board = WINDOW_SIZE;
	
	for(int x = 0; x < im->getWidth()-rigth_board; x++)
	{
		for(int y = 0; y < im->getHeight()-rigth_board; y++)
		{
			qCount[qClassify(im, x, y)]++;
		}
	}
	for(int i=0; i < 6; i++) printf("Q%d = %d\n", i, qCount[i]);
	
	return (qCount[1] - qCount[3])/4;
}

void binarisation(png_img * im, int threshold)
{
	for(int y = 0; y < im->getHeight(); y++)
	for(int x = 0; x < im->getWidth();  x++)
	im->setPixel(((im->getPixel(x,y) > threshold)? 255: 0), x, y);
}

int main(int argc, char **argv)
{
	printf("poly-test..\n");
	png_img poly_im(128, 128, 0, 8);
	int up_left_cornerX = 50;
	int up_left_cornerY = 50;
	for(int i = 0; i < poly_im.getWidth(); i++)
	for(int j = 0; j < poly_im.getHeight(); j++)
		poly_im.setPixel(0, i, j);
	
	for(int i = up_left_cornerX; i < up_left_cornerX + POLY_WIDTH; i++)
	for(int j = up_left_cornerY; j < up_left_cornerY + POLY_HEIGHT; j++)
		poly_im.setPixel(255, i, j);

	poly_im.write_png_file(filename_poly);
	int i_amount = connected_domain4_counting(&poly_im); 
	printf("Four-connected domain amount: %d\n", i_amount);
	
	printf("Read from file..\n");
	
	png_img im1(filename);
	binarisation(&im1, GLOBAL_THRESHOLD);
	int amount = connected_domain4_counting(&im1); 
	printf("Four-connected domain amount: %d\n", amount);
	printf("Save to files..\n");
	im1.write_png_file	(	filename_out		);
	printf("Done!\n");
}
