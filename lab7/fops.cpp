#include "fops.h"

png_img::png_img(int height, int width,	png_byte color_type, png_byte bit_depth): width(width), height(height), color_type(color_type), bit_depth(bit_depth)
{
	row_pointers = (unsigned char **)calloc(height, sizeof(unsigned char*));
	for(int i=0; i < height; i++)
	{
		row_pointers[i] = (unsigned char*) calloc(width, sizeof(unsigned char));
	}
}

png_img::png_img(char const *filename)
{
	read_png_file(filename);
}

void png_img::make_deep_copy(const png_img & im)
{
	row_pointers = (unsigned char **)calloc(height, sizeof(unsigned char*));
	for(int i=0; i < height; i++)
	{
		row_pointers[i] = (unsigned char*) calloc(width, sizeof(unsigned char));
	}
	for(int y=0; y < height; y++)
	{
		for(int x=0; x < width; x++)
		{
			row_pointers[y][x] = im.getPixel(x,y);
		}
	}
	
}

png_img::png_img(png_img const& im)
	: height(im.height), width(im.width), bit_depth(im.bit_depth), color_type(im.color_type) 
{
	make_deep_copy(im);
}

png_img::~png_img()
{
	for(int i=0; i < height; i++)
	{
		free(row_pointers[i]);
	}
	free(row_pointers);
}


png_img & png_img::operator = ( const png_img & im )
{
	if ( this == &im ) return *this; // присвоение самому себе, ничего делать не надо
        
	for(int i=0; i < height; i++) free(row_pointers[i]);
	free(row_pointers);
 
	make_deep_copy(im); 
        return *this;
}

void png_img::showInfo()
{
	printf("bit depth %d\n color type %d\n", bit_depth, color_type);
	printf("height %d width %d\n", height, width);
}

void png_img::read_png_file(char const *file_name)
{
	int x,y;
	png_structp png_ptr;
	png_infop info_ptr;
	
	int number_of_passes;
        char header[8];    // 8 is the maximum size that can be checked

        /* open file and test for it being a png */
        FILE *fp = fopen(file_name, "rb");
        if (!fp)  perror("[read_png_file] File could not be opened for reading\n");
      
	  fread(header, 1, 8, fp);

        /* initialize stuff */
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr) perror("[read_png_file] png_create_read_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr) perror("[read_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr))) perror("[read_png_file] Error during init_io");

        png_init_io(png_ptr, fp);
        png_set_sig_bytes(png_ptr, 8);

        png_read_info(png_ptr, info_ptr);

        width = png_get_image_width(png_ptr, info_ptr);
        height = png_get_image_height(png_ptr, info_ptr);
        color_type = png_get_color_type(png_ptr, info_ptr);
        bit_depth = png_get_bit_depth(png_ptr, info_ptr);

        number_of_passes = png_set_interlace_handling(png_ptr);
        png_read_update_info(png_ptr, info_ptr);


        /* read file */
        if (setjmp(png_jmpbuf(png_ptr))) perror("[read_png_file] Error during read_image");

        row_pointers = (unsigned char**) malloc(sizeof(unsigned char*) * height);
        for (y=0; y<height; y++)
                row_pointers[y] = (unsigned char*) malloc(png_get_rowbytes(png_ptr,info_ptr));

        png_read_image(png_ptr, row_pointers);

        fclose(fp);
}


void png_img::write_png_file(char const *file_name)
{
	int x,y;
	png_structp png_ptr;
	png_infop info_ptr;
	
        /* create file */
        FILE *fp = fopen(file_name, "wb");
        if (!fp) perror("[write_png_file] File could not be opened for writing\n");


        /* initialize stuff */
        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr) perror("[write_png_file] png_create_write_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr) perror("[write_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr))) perror("[write_png_file] Error during init_io");

        png_init_io(png_ptr, fp);


        /* write header */
        if (setjmp(png_jmpbuf(png_ptr))) perror("[write_png_file] Error during writing header");

        png_set_IHDR(png_ptr, info_ptr, width, height,
                     bit_depth, color_type, PNG_INTERLACE_NONE,
                     PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

        png_write_info(png_ptr, info_ptr);


        /* write bytes */
        if (setjmp(png_jmpbuf(png_ptr))) perror("[write_png_file] Error during writing bytes");

        png_write_image(png_ptr, row_pointers);


        /* end write */
        if (setjmp(png_jmpbuf(png_ptr))) perror("[write_png_file] Error during end of write");

        png_write_end(png_ptr, NULL);


        fclose(fp);
}

void png_img::setPixel(double val, int x, int y)
{
	unsigned char value = (unsigned char) val;
	if((x >= width) || (y >=height)) return;
	if((x < 0) || (y < 0)) return;
	unsigned char* row  = row_pointers[y];
	row[x] = value;
}

unsigned char png_img::getPixel(int x, int y) const
{
	if((x >= width) || (y >= height)) return 0;
	if((x < 0) || (y < 0)) return 0;
	unsigned char* row = row_pointers[y];
	return row[x];
}

int png_img::getHeight()
{
	return height;
}

int png_img::getWidth()
{
	return width;
}
