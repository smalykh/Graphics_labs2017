#include <stdio.h>
#include "fops.h"
#include <math.h>
#include <vector>
#include <algorithm>
using namespace std;

#define BIT_DEPTH 	8
#define COLORTYPE 	0


const char *filename		= "impulse.png";
const char *filename_out	= "file1_out.png";

#define KERN_SIZE		3
const int mask[KERN_SIZE][KERN_SIZE] =
	{
		1, 2, 1,
		2, 1, 2,
		1, 2, 1
	};

int median(png_img *im, int x, int y)
{
	int board = KERN_SIZE/2;
	vector<int> vals;
	for(int i = 0; i < KERN_SIZE; i++)	
	{
		for(int j = 0; j < KERN_SIZE; j++)	
		{
			for(int n=0; n < mask[i][j]; n++)
			 vals.push_back(im->getPixel(x + i - board, y + j - board));
		}
	}
	sort(vals.begin(), vals.end());
//	for(int i =0; i < vals.size(); i++) printf("%d ", vals[i]);
//	printf("\n");
	return vals[KERN_SIZE*KERN_SIZE/2];
}

png_img * med_filter(png_img * im)
{
	png_img *result = new png_img(*im);

	int board = KERN_SIZE/2;
	for(int x = board; x < im->getWidth() - board; x++)
	{
		for(int y = board; y < im->getHeight() - board; y++)
		{
			result->setPixel(median(im, x, y), x, y);
		}
	}
	return result;
}

int main(int argc, char **argv)
{
	printf("Read from file..\n");
	
	png_img im1(filename);

	png_img *im2 = med_filter(&im1); 

	printf("Save to files..\n");
	im2->write_png_file	(	filename_out		);
	printf("Done!\n");
}
