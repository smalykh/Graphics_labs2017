#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#define PNG_DEBUG 3
#include <png.h>


class png_img
{
private:
	// Private values for properties
	int width;
	int height;
	unsigned char **row_pointers;
	
	// For lpng functions correct work
	png_byte color_type;
	png_byte bit_depth;
	
	void make_deep_copy(png_img const &im);
	void read_png_file(char const* filename);
public:
	int getWidth();
	int getHeight();
	void showInfo();

	unsigned char getPixel(int x, int y) const;
	void  setPixel(double val, int x, int y);
	png_img &operator = ( const png_img &a ); 
	void write_png_file(char const* filename);
	
	png_img(int heigth, int weigth,	png_byte color_type, png_byte bit_depth);
	png_img(char const *filename);
	png_img(png_img const& im);

	~png_img();
};

