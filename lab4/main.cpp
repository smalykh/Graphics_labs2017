#include <stdio.h>
#include "fops.h"
#include <math.h>
using namespace std;

#define BIT_DEPTH 	8
#define COLORTYPE 	0


const char *filename		= "lena.png";
const char *filename1_out	= "file1_out.png";
const char *filename2_out	= "file2_out.png";

void AutoContrastGray(png_img *im, int HT, int CT, double gamma,int d1,int d2, int isGamma = 0) 
{
	unsigned long Hist[256];
	unsigned char ColorTable[256];
	int i, lLeftLim, lRightLim;

	int height = im->getHeight();
	int width = im->getWidth();

	memset(Hist, 0, 256*sizeof(long));

	for (int y = 0; y <height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			Hist[im->getPixel(x,y)]++;
		}
	}
	
	for (lLeftLim = 0; lLeftLim < 150; lLeftLim++)
	{
		if (Hist[lLeftLim] > HT) break;
	}

	for (lRightLim = 255; lRightLim > 150; lRightLim--)
	{
		if (Hist[lRightLim] > HT) break;
   	}
	for(i=0;i<lLeftLim;i++) ColorTable[i]=(unsigned char)d1;

	for (i = lLeftLim; i < lRightLim; i++)
	{
		int tmp;
		if(isGamma) tmp = pow(255*(i-lLeftLim)/(lRightLim-lLeftLim),1/gamma)*(d2-d1)/255+d1;
        	else tmp = (unsigned char)(255*(i-lLeftLim)/(lRightLim-lLeftLim));
		ColorTable[i]=(unsigned char)(tmp);
	}
    
	for(i=lRightLim;i<256;i++) ColorTable[i]=(unsigned char)d2;

	for (int y = 0; y < height; y++) 
	for (int x = 0; x < width; x++)
	{
		im->setPixel(ColorTable[im->getPixel(x, y)], x, y);
	}

}




int main(int argc, char **argv)
{
	printf("Read from file..\n");
	
	png_img im1(filename);
	png_img im2(filename);

	AutoContrastGray(&im1, im1.getWidth()*im1.getHeight()*0.005/2, 5050, 1, 0, 250);
//	AutoContrastGray(&im2, im2.getWidth()*im2.getHeight()*0.005/2, 5050, 1, 0, 250, 1);

	printf("Save to files..\n");
	im1.write_png_file	(	filename1_out		);
//	im2.write_png_file	(	filename2_out		);
	printf("Done!\n");
}
