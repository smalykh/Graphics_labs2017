#include <stdio.h>
#include "fops.h"

#define BIT_DEPTH 	8
#define COLORTYPE 	0

const char *filename = "lena.png";
const char *filename_out = "file2_out.png";

double interpol(double A, double B, double t)
{
    double a = -A / 2.0 + B / 2.0;
    double b = B;
    return a*t+b;
}

int bilinear_interpolation(png_img *im, double u, double v)
{
    double x = (u * im->getWidth()) - 0.5;
    int xint = (int)x;
    double dx = x - xint;
    
    double y = (v * im->getHeight()) - 0.5;
    int yint = (int)y;
    double dy = y - yint;
    
    int p00 = im->getPixel(xint, yint);
    int p10 = im->getPixel(xint + 1, yint);
    
    int p02 = im->getPixel( xint, yint + 1);
    int p12 = im->getPixel( xint + 1, yint + 1);
    
    double c1 = interpol(p00, p10, dx);
    double c2 = interpol(p02, p12, dx);
    
    double value  = interpol(c1, c2, dy);
    value = (value > 0  )? value: 0;	
    value = (value < 255)? value: 255;
	
    return value;

}

double cubic_interpolation(double *points, double t)
{
	double a = -points[0]/2.0 + 3.0*points[1]/2.0 - 3.0*points[2]/2.0 + points[3]/2.0;
	double b =  points[0] - 5.0*points[1]/2.0 + 2.0*points[2] - points[3]/2.0;
	double c = -points[0]/2.0 + points[2]/2.0;
	double d =  points[1];

	return a*t*t*t + b*t*t + c*t + d;
}

unsigned char bicubic_interpolation(png_img *im, double u, double v)
{
	double x = (u * im->getWidth()  - 0.5);
	double y = (v * im->getHeight() - 0.5);

	int xint = (int)x;
	int yint = (int)y;

	double dx = x - (int)xint; 
	double dy = y - (int)yint; 
	
	// Neighbour pixels
	double pixels[4];
	int p;
	// Results of cubic interpoaltion
	double c[4];

	for(int i = 0; i <= 3; i++)
	{
		for(int j = 0; j <=3; j++)
		{
			p = im->getPixel(xint + j-1, yint + i-1);
			pixels[j] = (double)p;
		}
		c[i] = cubic_interpolation(pixels, dx);
	}
	double value = cubic_interpolation(c, dy);
	
	// Check oveflow
	value = (value > 0  )? value: 0;	
	value = (value < 255)? value: 255;
	
	return value;
}
enum Transform_type {BILINEAR, BICUBIC};

png_img* bicubic_transform(png_img *im, Transform_type type, double N)
{
	png_img * tr_img = new png_img(N*im->getHeight(), N*im->getWidth(), COLORTYPE, BIT_DEPTH);

	for(int y = 0; y < tr_img->getHeight(); y++)
	{
		for(int x = 0; x < tr_img->getWidth(); x++)
		{
			int val;
			if(type == BILINEAR) val = bilinear_interpolation(im, x/(double)(tr_img->getWidth() - 1) , y/(double)(tr_img->getHeight() - 1));
			else 		     val = bicubic_interpolation(im, x/(double)(tr_img->getWidth() - 1) , y/(double)(tr_img->getHeight() - 1));
			tr_img->setPixel(val, x, y);
		}
	}

	return tr_img;
}

int main(int argc, char **argv)
{
	printf("Read from file %s..\n", filename);
	png_img im(filename);
	im.showInfo();

	printf("Enter N:\n");
	double N;
	if(!scanf("%lf", &N)) 
	{
		fprintf(stderr, "Wrong argument!");
		return -1;
	}
	
	//png_img *tr_im = bicubic_transform(&im, BICUBIC, N);

	png_img *tr_im = bicubic_transform(&im, BILINEAR, N);

	printf("Save to file %s..\n", filename_out);
	tr_im->write_png_file(filename_out);
	printf("Done!\n");
}
