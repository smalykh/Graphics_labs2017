#include <stdio.h>
#include "fops.h"
#include <math.h>
using namespace std;

#define BIT_DEPTH 	8
#define COLORTYPE 	0


const char *filename		= "lena.png";
const char *filename_out	= "file1_out.png";

#define KERN_SIZE		3
const double KERN_SUM = 1;
const double kernel[KERN_SIZE][KERN_SIZE]=
//    {
//        0, 1, 1, 1, 0,
//        1, 1, 1, 1, 1,
//        1, 1, 1, 1, 1,
//        1, 1, 1, 1, 1,
//        0, 1, 1, 1, 0
///    };
	{
	  	 0, -1,  0,
		-1, 4, -1,
		 0, -1,  0
	};

int kernel_mult(png_img *im, int x, int y)
{
	int board = KERN_SIZE/2;
	int sum = 0;
	for(int i = 0; i < KERN_SIZE; i++)	
	{
		for(int j = 0; j < KERN_SIZE; j++)	
		{
			sum += kernel[i][j] * im->getPixel(x + i - board, y + j - board);
		}
	}
	return sum/KERN_SUM;
}

png_img * convolution(png_img * im)
{
	png_img *result = new png_img(*im);

	int board = KERN_SIZE/2;
	for(int x = board; x < im->getWidth() - board; x++)
	{
		for(int y = board; y < im->getHeight() - board; y++)
		{
			result->setPixel(kernel_mult(im, x, y), x, y);
		}
	}
	return result;
}

int main(int argc, char **argv)
{
	printf("Read from file..\n");
	
	png_img im1(filename);

	png_img *im2 = convolution(&im1); 

	printf("Save to files..\n");
	im2->write_png_file	(	filename_out		);
	printf("Done!\n");
}
