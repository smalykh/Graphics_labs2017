#include <stdio.h>
#include <random>
#include "fops.h"

using namespace std;

#define BIT_DEPTH 	8
#define COLORTYPE 	0

#define MEAN		0
#define	STD		30

const char *filename1		= "lena.png";
const char *filename2 		= "lena.png";
const char *filename_alpha 	= "lena.png";
const char *filename_out	= "file_out.png";
const char *filename_AWGN	= "AWGN.png";
const char *filename_impulse	= "impulse.png";
const char *filename_corr	= "corr.png";

double randn(double mean, double std)
{
	random_device rd;
	mt19937 gen(rd());
	normal_distribution<double> d(mean, std);
    
	return d(gen);
}
void add_AWGN(png_img *im)
{
	for (int y=0; y<im->getHeight(); y++) 
	{
		for (int x=0; x < im->getWidth(); x++) 
		{
			im->setPixel(im->getPixel(x,y) + randn(MEAN, STD), x, y);	
        	}
    	}
}

void add_noise(png_img *im)
{
	for (int y=0; y<im->getHeight(); y++) 
	{
		int sum_err = 0;
		for (int x=0; x < im->getWidth(); x++) 
		{ 
			int err = randn(MEAN, STD);
			sum_err += abs(err);
			im->setPixel(im->getPixel(x,y) + sum_err/4, x, y);	
        	}
    	}
}


void add_impulse_noise (png_img *im, double percent)
{
	double number_of_change_pix = (double)(im->getWidth()*im->getHeight())*percent;
	srand (time(NULL));
    
	for (int i=0; i<number_of_change_pix; i++) 
	{
		int x_rand = rand() % im->getWidth();
		int y_rand = rand() % im->getHeight();
		im->setPixel((rand() % 2)? 0 : 255, x_rand, y_rand);
        }
}

png_img* imageBlending(png_img * im1, png_img *im2, png_img *alpha)
{
    
	if (im1->getWidth()  != im2->getWidth()   )	return NULL;
	if (im1->getWidth()  != alpha->getWidth() ) 	return NULL;
	if (im1->getHeight() != im2->getHeight()  )	return NULL;
	if (im1->getHeight() != alpha->getHeight())	return NULL;

    	int height = im1->getHeight();
	int width  = im1->getWidth();

	png_img *result = new png_img(height, width, COLORTYPE, BIT_DEPTH);
    
	for (int y=0; y<height; y++) 
	{
 		for (int x=0; x<width; x++) 
		{
            		int p1 = im1->getPixel(x, y);
            		int p2 = im2->getPixel(x, y);
            		int a  = alpha->getPixel(x, y);
           		a = 255-a;
            		int pix = p2 + (p1 - p2)*a/255;
           		result->setPixel(pix, x, y);
		}
        }

	return result;
}


int main(int argc, char **argv)
{
	printf("Read from files..\n");
	
	png_img im1	(filename1);
	png_img im2	(filename2);
//	png_img alpha	(filename_alpha);

	png_img awgn	(filename1);
	png_img impulse	(filename1);
	png_img im_noise(filename1);

//	png_img *tr_im = imageBlending(&im1, &im2, &alpha);
	printf("Creating AWGN..\n");
	add_AWGN(&awgn);
	printf("Creating impulse noise..\n");
	add_impulse_noise(&impulse, 0.02);

	printf("Creating corr noise..\n");
	add_noise(&im_noise);

	printf("Save to files..\n");
//	tr_im->write_png_file	(	filename_out		);
	awgn.write_png_file	(	filename_AWGN		);
	impulse.write_png_file	(	filename_impulse	);
	im_noise.write_png_file  (	filename_corr		);
	printf("Done!\n");
}
