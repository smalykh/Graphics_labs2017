#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#define PNG_DEBUG 3
#include <png.h>

int x, y;

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
int number_of_passes;
unsigned char **row_pointers;

void read_png_file(char* file_name)
{
        char header[8];    // 8 is the maximum size that can be checked

        /* open file and test for it being a png */
        FILE *fp = fopen(file_name, "rb");
        if (!fp)  perror("[read_png_file] File could not be opened for reading\n");
      
	  fread(header, 1, 8, fp);

        /* initialize stuff */
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr) perror("[read_png_file] png_create_read_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr) perror("[read_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr))) perror("[read_png_file] Error during init_io");

        png_init_io(png_ptr, fp);
        png_set_sig_bytes(png_ptr, 8);

        png_read_info(png_ptr, info_ptr);

        width = png_get_image_width(png_ptr, info_ptr);
        height = png_get_image_height(png_ptr, info_ptr);
        color_type = png_get_color_type(png_ptr, info_ptr);
        bit_depth = png_get_bit_depth(png_ptr, info_ptr);

        number_of_passes = png_set_interlace_handling(png_ptr);
        png_read_update_info(png_ptr, info_ptr);


        /* read file */
        if (setjmp(png_jmpbuf(png_ptr))) perror("[read_png_file] Error during read_image");

        row_pointers = (unsigned char**) malloc(sizeof(unsigned char*) * height);
        for (y=0; y<height; y++)
                row_pointers[y] = (unsigned char*) malloc(png_get_rowbytes(png_ptr,info_ptr));

        png_read_image(png_ptr, row_pointers);

        fclose(fp);
}


void write_png_file(char* file_name)
{
        /* create file */
        FILE *fp = fopen(file_name, "wb");
        if (!fp) perror("[write_png_file] File could not be opened for writing\n");


        /* initialize stuff */
        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr) perror("[write_png_file] png_create_write_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr) perror("[write_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr))) perror("[write_png_file] Error during init_io");

        png_init_io(png_ptr, fp);


        /* write header */
        if (setjmp(png_jmpbuf(png_ptr))) perror("[write_png_file] Error during writing header");

        png_set_IHDR(png_ptr, info_ptr, width, height,
                     bit_depth, color_type, PNG_INTERLACE_NONE,
                     PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

        png_write_info(png_ptr, info_ptr);


        /* write bytes */
        if (setjmp(png_jmpbuf(png_ptr))) perror("[write_png_file] Error during writing bytes");

        png_write_image(png_ptr, row_pointers);


        /* end write */
        if (setjmp(png_jmpbuf(png_ptr))) perror("[write_png_file] Error during end of write");

        png_write_end(png_ptr, NULL);

        /* cleanup heap allocation */
        for (y=0; y<height; y++)
                free(row_pointers[y]);
        free(row_pointers);

        fclose(fp);
}

void set_pixel(double val, int x, int y)
{
	unsigned char value = (unsigned char) val;
	if((x >= width) || (y >=height)) return;
	if((x < 0) || (y < 0)) return;
	unsigned char* row  = row_pointers[y];
	row[x] = value;
}
unsigned char get_pixel(int x, int y)
{
	if((x >= width) || (y >= height)) return 0;
	if((x < 0) || (y < 0)) return 0;
	unsigned char* row = row_pointers[y];
	return row[x];
}
unsigned char check_overflow(double val)
{
	if (val > 255.0) return  255;
	if (val < 0.0)	 return 0;
	return (unsigned char) val;
}
unsigned char get_quant_val(unsigned char val)
{
	if(val >= 192) return 255;
	if(val >= 128) return 192;
	if(val >= 64)  return 64;
	return 0;
}
void process_file(void)
{
        for (y=0; y<height; y++) 
	{
                for (x=0; x<width; x++) 
		{
			unsigned char old_pix = get_pixel(x,y);
			unsigned char new_pix = get_quant_val(old_pix); 
			set_pixel(new_pix, x, y);
			double q_error = old_pix - new_pix;

			set_pixel(check_overflow(get_pixel(x + 1, y    ) + q_error * 7/16), x + 1, y); 
			set_pixel(check_overflow(get_pixel(x - 1, y + 1) + q_error * 3/16), x - 1, y + 1);
			set_pixel(check_overflow(get_pixel(x    , y + 1) + q_error * 5/16), x    , y + 1); 
			set_pixel(check_overflow(get_pixel(x + 1, y + 1) + q_error * 1/16), x + 1, y + 1); 
                }
        }
}


int main(int argc, char **argv)
{
        if (argc != 3)
                perror("Usage: program_name <file_in> <file_out>");

        read_png_file(argv[1]);
        process_file();
        write_png_file(argv[2]);

        return 0;
}
